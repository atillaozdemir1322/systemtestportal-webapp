/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package system

import (
	"time"
)

const DatePickerLayout string = "01/02/2006"

/*
The SystemSettings are an automated XORM-struct. This means any changes you make to the struct will be automatically
applied to the table in the database. It also stores multiple fields of date information, which are explained as follows:

DATE FIELDS ---------------------------------------------------------------------------------|
GlobalMessageType:
	Saves an css class, which is inserted into the navbar in the template.

GlobalMessageTimeStamp:
	Saves the date a message was last set to display. This is used so when a new message is set,
	users will see it again (If they had clicked away the previous message).

GlobalMessageExpirationDate:
	Saves the date a message is going to expire on as a go-time field. We use this to verify
	the user-input date is valid

GlobalMessageExpirationDateUnix:
	Saves the date a message is going to expire as a Unix int. This just means we don't
	have to calculate it frontend everytime we wan't to check the date.
---------------------------------------------------------------------------------------------|
*/
type SystemSettings struct {
	Id int64

	IsDisplayGlobalMessage        bool
	IsAccessAllowed               bool
	IsRegistrationAllowed         bool
	IsEmailVerification           bool
	IsDeleteUsers                 bool `xorm:"BIT(1) NOT NULL DEFAULT 1"`
	IsGlobalMessageExpirationDate bool
	IsExecutionTime               bool

	GlobalMessage                   string
	GlobalMessageType               string
	GlobalMessageTimeStamp          string
	GlobalMessageExpirationDate     time.Time
	GlobalMessageExpirationDateUnix string

	ImprintMessage string
	PrivacyMessage string
}

func (systemSettings *SystemSettings) GetExpirationDateForDatePicker() string {
	return systemSettings.GlobalMessageExpirationDate.Format(DatePickerLayout)
}
