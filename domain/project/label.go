/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

// A Label is used to group tests
type Label struct {
	Id 			int64

	// Foreign Key ----------|
	ProjectId       int64
	//-----------------------|

	Name        string
	Description string
	Color       string
	TextColor	string
}

type TestLabel struct {
	Id		int64

	// Foreign Key ----------|
	LabelId		int64
	TestId  	int64
	ProjectId 	int64
	//-----------------------|

	IsCase bool `xorm:"not null default 0"`
}