/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package task

// itemReference is a unique reference to the entity that is linked to
// a task-item. The entity can be uniquely identified by the type (case, seqence, ...)
// and an ID.
type itemReference struct {
	ID   string
	Type ReferenceType
}

// ReferenceType is the type of the entity that
// is referenced by the task-item
type ReferenceType int

// These are the possible reference-types
const (
	Case ReferenceType = iota
	Sequence
	SUTVersion
)
