{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Member List" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="2">
                            <ul>
                                <li>{{T "See the list of members in this project." .}}</li>
                            {{ if or .Member .Admin }}
                                <li>{{T "Add new members (see button below)." .}}</li>
                            {{ end }}
                            {{ if or .Owner .Admin}}
                                <li>{{T "Remove members (see button below)." .}}</li>
                            {{ end }}
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h5>Buttons</h5></td>
                    </tr>
                {{ if .ProjectPermissions.EditMembers }}
                    <tr>
                        <td>
                            <button class="order-3 btn btn-primary align-middle">
                                + <span class="d-none d-sm-inline">{{T "Add member" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Add a user as member to the project." .}}</td>
                    </tr>
                {{ end }}
                {{ if or .Owner .Admin}}
                    <tr>
                        <td>
                            <button class="order-3 btn btn-primary align-middle">
                                <span class="fa fa-sign-out"></span> <span
                                    class="d-none d-sm-inline">{{T "Remove member" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Remove a users membership in this project." .}}</td>
                    </tr>
                {{ end }}
                </table>
                <span class="mt-3 float-left">For more information visit our <a href="http://docs.systemtestportal.org" target="_blank">documentation</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
{{ template "modal-member-assignment" . }}
{{ template "modal-member-remove" . }}
{{ template "remove-abort-modal" . }}
<div class="tab-card card" id="tabMembers">
    <nav class="navbar-light action-bar p-3">
    {{ if or .ProjectPermissions.EditMembers .Admin }}
        <button class="order-3 btn btn-primary align-middle mr-2 float-left" id="buttonAddMember"
                data-toggle="modal" data-target="#modal-member-assignment">
            + <span class="d-none d-sm-inline">{{T "Add member" .}}</span>
        </button>
    {{ end }}
    {{ if or .Owner .Admin}}
        <button class="order-3 btn btn-secondary align-middle float-left" id="buttonRemoveMember"
                data-toggle="modal" data-target="#modal-member-remove">
            <span class="fa fa-sign-out"></span> <span class="d-none d-sm-inline">{{T "Remove member" .}}</span>
        </button>
    {{ end }}
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-md-12 p-3">
            <h4 class="mb-3">{{T "Members" .}}</h4>
            <ul class="list-group" id="membersList">
            {{ $roles := .Project.Roles }}
            {{ $editPermission := .ProjectPermissions.EditPermissions }}
            {{ range .Members }}
                <li class="list-group-item memberLine" id="{{ .User }}">
                    <div class="row">
                        <a href="/users/{{.User}}">
                            <img src="{{getUserImageFromID .User}}" alt="" class="rounded-circle profile-picture" height="42" width="42">
                        </a>
                        <div class="col-4 col-lg-8">
                        {{ .User }}
                    {{ $userRole := .Role}}
                        </div>
                    {{ if ($editPermission)}}
                        <div class="col-7 col-lg-3 text-right">
                        {{ if ne ($userRole) ("Owner")}}
                            <select class="bootstrap-select custom-select" title="Select Role"
                                    id="{{.User}}roleSelector"
                                    onChange="roleSelection({{.User}}, id, this.selectedIndex)">

                            {{ range $index, $role := $roles }}
                            {{ if ne (.Name) ("Owner")}}
                                <option value="{{ $index }}" {{if eq ($userRole) (.Name) }}
                                        selected="selected" {{ end }}>{{ .Name }}</option>
                            {{end}}
                            {{ end }}
                            </select>
                        {{else}}
                            Owner
                        {{end}}
                        </div>
                    {{ else }}
                        <div class="col text-right">
                        {{ .Role }}
                        </div>
                    {{ end }}
                    </div>
                </li>
            {{ end }}
            </ul>
        </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/util/common.js" integrity="{{sha256 "/static/js/util/common.js"}}"></script>
<script src='/static/js/modal/select-members.js'></script>

<script>
    $("#printerIcon").addClass("d-none");
    $("#helpIcon").removeClass("d-none");

    /**
     * Keyboard support
     */
    $(document)
            .off("keydown.event")
            .on("keydown.event", function (event) {
                if($('.modal .active').length > 0){
                    return;
                }

                switch (event.target.tagName) {
                    case "INPUT": case "SELECT": case "TEXTAREA": return;
                }
        let buttonString;
        switch (event.key) {
            case "+":
                buttonString = "buttonAddMember";
                break;
            case "R":
            case "r":
                buttonString = "buttonRemoveMember";
                break;
        }
        if (document.getElementById(buttonString)){
            document.getElementById(buttonString).click();
        }

    });
</script>
{{end}}