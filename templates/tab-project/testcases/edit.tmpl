{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<link rel="stylesheet" type="text/css" href="/static/css/project/reorder-utils.css" integrity="{{sha256 "/static/css/project/reorder-utils.css"}}">
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Test Case Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            {{T "In this view you can change the details of a test case" .}}.
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Abort" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Abort editing and get back to the test case details" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>Esc</button></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-success" type="button">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Save" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Confirm changes, save them and return back to the details" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>S</button></td>
                    </tr>
                    <tr>
                        <td colspan="3"><h5>Form Fields</h5></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Case Name" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Enter a short but descriptive name for the test case" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Case Description" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Describe the purpose of that test case in some sentences" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Case Preconditions" .}}</strong>
                        </td>
                        <td colspan="2">{{T "State some preconditions that must be fulfilled prior to test case execution" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Test Steps" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Add some test steps here" .}}.</td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Variants & Versions" .}}</strong>
                        </td>
                        <td colspan="2">{{T "Select some versions under which the test case can be executed for each relevant variant" .}}.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{T "Estimated Test Time" .}} (hh:mm)</strong>
                        </td>
                        <td colspan="2">{{T "Enter an estimated amount of time that is needed to execute the whole test" .}}.</td>
                    </tr>
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-testCase-save" tabindex="-1" role="dialog"
     aria-labelledby="modal-testCase-save-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-testCase-save-label">{{T "Save Test Case" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <!-- don't break the following two lines, otherwise the placeholder will not be displayed properly-->
                    <div class="md-area">
                    <textarea class="form-control markdown-textarea" id="inputCommitMessage" rows="4"
                              placeholder="Why did you edit the test case? (Optional)"></textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">{{T "Markdown supported" .}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="checkbox mr-auto">
                    <label><input type="checkbox" id="minorUpdate">
                        <abbr title="This is only a minor edit">{{T "Minor Change" .}}</abbr>
                    </label>
                </div>
                <button id="buttonSave" type="button" class="btn btn-primary">{{T "Save" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-testStep" tabindex="-1" role="dialog" aria-labelledby="modal-testStep-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-testStep-label">Test Step</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body" id="testStep">
                <div class="form-group">
                            <textarea class="form-control" id="inputTestStepAction" rows="4"
                                      placeholder="{{T "What action shall be performed?" .}}"></textarea>
                </div>
                <div class="form-group">
                            <textarea class="form-control" id="inputTestStepExpectedResult" rows="4"
                                      placeholder="{{T "Which result is expected?" .}}"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button id="buttonSaveTestStep" type="button" class="btn btn-primary">{{T "Save" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="tab-card card" id="tabTestCases">
    <form id="testCaseEdit">
        <script>
            var caselist = {{.TestCases}};
            var cases = [];
            for (testCase of caselist) {
                if(testCase !== {{ .TestCase.Name }}) cases.push(testCase);
            }
        </script>
        <nav class="navbar navbar-light action-bar p-3">
            <div class="input-group flex-nowrap">
                <button class="btn btn-secondary" id="buttonAbortEdit">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> {{T "Abort" .}}</span>
                </button>
                <button id="buttonSaveTestCase" class="btn btn-success ml-2" data-toggle="modal"
                        onclick="checkForChanges()" type="button" disabled>
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                    <span class="d-none d-sm-inline"> {{T "Save" .}}</span>
                </button>
            </div>
        </nav>
        <div class="row tab-side-bar-row">
            <div class="col-md-9 p-3">
                <h4 class="mb-3">Edit {{ .TestCase.Name }}</h4>
                <div class="form-group">
                    <label for="inputTestCaseName"><strong>{{T "Test Case Name" .}}</strong></label>
                    <input class="form-control" id="inputTestCaseName" name="inputTestCaseName"
                           placeholder="{{T "Enter test case name" .}}" value="{{ .TestCase.Name }}" required>
                </div>
            {{ with .TestCaseVersion }}
                <div class="form-group">
                    <label for="inputTestCaseDescription"><strong>{{T "Test Case Description" .}}</strong></label>
                    <div class="md-area">
                        <textarea class="form-control markdown-textarea" id="inputTestCaseDescription" rows="4"
                                  placeholder="{{T "Describe the test case" .}}">{{ .Description }}</textarea>
                        <div class="md-area-bottom-toolbar">
                            <div style="float:left">{{T "Markdown supported" .}}</div>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label class="mt-2" for="inputTestCasePreconditions"><strong>{{T "Test Case Preconditions" .}}</strong></label>
                    {{/*<button type="button" class="btn btn-sm btn-primary float-right" id="preconditionAdder">*/}}
                        {{/*Add Condition*/}}
                    {{/*</button>*/}}

                   <ul class="list-group" id="preconditionsList">
                    {{ if .Preconditions }} 
                        {{ range $index, $elem := .Preconditions }}
                        <li class="list-group-item preconditionItem" >
                            <span>{{ $elem.Content }}</span>
                            <button class="close ml-2 list-line-i tem btn-sm deletePrecondition pull-right" type="button">
                                    <span class="d-none d-sm-inline">x</span>
                            </button>
                        </li>
                        {{ end }}
                    {{ end }} 
                    </ul>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary" id="preconditionAdder">
                                <i class="fa fa-plus"></i>
                            </button>
                        </span>
                        <input class="form-control width100" id="preconditionInput" placeholder="{{T "New Precondition" .}}">
                    </div>
                </div>

                <label class="mt-2"><strong>{{T "Test Steps" .}}</strong></label>
                <div class="form-group" id="test-steps-container">
                {{ if .Steps }}
                    <ul class="list-group sortable pb-2 pt-2" id="testStepsAccordion" data-children=".li">
                    {{ range $index, $element := .Steps }}
                        <li class="list-group-item pr-3" id="testStep{{ $index }}">
                            <div id="markdown-task-{{ $index }}" class="d-none markdown-task">{{.Action}}</div>
                            <div id="markdown-exre-{{ $index }}" class="d-none markdown-exre">{{.ExpectedResult}}</div>

                            <div class="d-flex">
                                <div>
                                    <button type="button" class="test-case-reorder">
                                        <i class="fa fa-bars"></i>
                                    </button>
                                </div>

                                <div style="flex: 1;">
                                    <span id="ActionField-{{ $index }}"  data-toggle="collapse" data-parent="#testStepsAccordion"
                                          href="#testStepsAccordion{{ $index }}"
                                          aria-expanded="true" aria-controls="testStepsAccordion{{ $index }}"> {{printMarkdown .Action }} </span>

                                    <div id="testStepsAccordion{{ $index }}" class="collapse" role="tabpanel">
                                        <span id="ExpectedResultField-{{ $index }}" class="text-muted">{{printMarkdown .ExpectedResult }}</span>
                                        <button type="button"
                                                class="btn btn-danger ml-2 list-line-item btn-sm buttonDeleteTestStep"
                                                id="{{ $index }}">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            <span class="d-none d-sm-inline"> {{T "Delete" .}}</span>
                                        </button>
                                        <button type="button"
                                                class="btn btn-danger ml-2 d-none list-line-item btn-sm buttonDeleteTestStepConfirm"
                                                id="{{ $index }}" data-toggle="modal"
                                                data-target="#deleteModal">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            <span class="d-none d-sm-inline"> {{T "Confirm delete" .}}</span>
                                        </button>
                                        <button type="button"
                                                class="btn btn-primary ml-auto list-line-item btn-sm buttonEditTestStep"
                                                id="{{ $index }}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            <span class="d-none d-sm-inline"> {{T "Edit" .}}</span>
                                        </button>
                                    </div>
                                </div>

                            </div>

                        </li>
                    {{ end }}
                    </ul>
                {{ else }}
                    <p class="text-muted">No Test Steps</p>
                    <ol class="list-group" id="testStepsAccordion" data-children=".li"></ol>
                {{ end }}
                {{template "modal-teststep-edit" .}}
                </div>
                <div>
                    <button type="button" class="btn btn-sm btn-primary float-right" id="buttonAddTestStep" data-toggle="modal" data-target="#modal-teststep-edit">
                        {{T "Add Test Step" .}}
                    </button>
                </div>
            {{ end }}
            </div>
            <div class="col-md-3 p-3 tab-side-bar">
                <div class="form-group">
                    <label><strong>{{T "Applicability" .}}</strong>
                        <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal"
                                data-target="#modal-manage-versions">
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                        </button>
                    </label>
                {{ if .Project.Versions }}
                <div id="someversions" class="form-group">
                {{ else }}
                <div id="someversions" class="form-group d-none">
                {{ end }}
                    <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                        <label for="inputTestCaseSUTVersions">
                            <strong>{{T "Versions" .}}</strong>
                        </label>
                    </div>
                    <div class="col-10 col-sm-11 col-md-12 col-lg-12">
                        <select class="custom-select mb-2" id="inputTestCaseSUTVersions">
                        </select>
                    </div>
                    <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                        <label for="inputTestCaseSUTVariants">
                            <strong>{{T "Variants" .}}</strong>
                        </label>
                    </div>
                    <div class="form-group col-10 col-sm-11 col-md-12 col-lg-12">
                        <select multiple class="form-control" id="inputTestCaseSUTVariants">
                        </select>
                    </div>
                </div>
                {{ if .Project.Versions }}
                <div id="noversion" class="form-group d-none">
                {{ else }}
                <div id="noversion" class="form-group">
                {{ end }}
                    <p class="text-muted">{{T "The project does not have any versions yet. Click on the icon above to manage the versions" .}}.</p>
                </div>
                </div>
                    <div class="form-group mt-4">
                        <label for="inputHours"><strong>{{T "Estimated Test Time" .}} (hh:mm)</strong></label>
                        <div class="row m-0">
                            <div class="col-lg-6 col-md-12 col-sm-3 col-6 p-0 pb-md-2">
                                <div class="input-group mb-2 mb-sm-0 ">
                                    <input type="number" class="form-control pt-0 pb-0" id="inputHours" placeholder="h"
                                           min="0" oninput="checkHours(this)" step="1"
                                    {{ if or (gt .DurationHours 0) (gt .DurationMin 0) }}
                                           value="{{ .DurationHours }}" {{ end }}>
                                    <div class="btn-group-vertical">
                                        <button type="button" id="hour-plus" class="input-group-addon btn btn-primary">
                                            <span>&#x2b;</span>
                                        </button>
                                        <button type="button" id="hour-minus" class="input-group-addon btn btn-primary">
                                            <span>&#x2212;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-3 col-6 p-0 pl-2 pl-md-0 pl-lg-2">
                                <div class="input-group mb-2 mb-sm-0 ">
                                    <input type="number" class="form-control pt-0 pb-0" id="inputMinutes"
                                           placeholder="min"
                                           min="0" max="59" oninput="checkMins(this)" step="1"
                                    {{ if or (gt .DurationHours 0) (gt .DurationMin 0) }}
                                           value="{{ .DurationMin }}" {{ end }}>
                                    <div class="btn-group-vertical">
                                        <button type="button" id="minute-plus"
                                                class="input-group-addon btn btn-primary">
                                            <span>&#x2b;</span>
                                        </button>
                                        <button type="button" id="minute-minus"
                                                class="input-group-addon btn btn-primary">
                                            <span>&#x2212;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{template "modal-manage-versions" . }}
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Import Scripts here -->
<script src="/static/js/project/testcases.js" integrity="{{sha256 "/static/js/project/testcases.js"}}"></script>
<script src="/static/js/util/common.js" integrity="{{sha256 "/static/js/util/common.js"}}"></script>
<script src="/static/js/project/sut-versions.js" integrity="{{sha256 "/static/js/project/sut-versions.js"}}"></script>
<script src="/static/js/project/tests.js" integrity="{{sha256 "/static/js/project/tests.js"}}"></script>
<script src="/static/js/project/sut-versions-show.js" integrity="{{sha256 "/static/js/project/sut-versions-show.js"}}"></script>
<script src="/static/assets/js/vendor/jquery.validate.min.js" integrity="{{sha256 "/static/assets/js/vendor/jquery.validate.min.js"}}"></script>

<script>
    $('document').ready(function() {
            $("#testCaseEdit").valid();
        }
    );
    /* Handler for enter-key*/
    $("#inputTestCaseName").keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            $("#inputTestCaseDescription").focus();
        }
    });

    assignEventsToTextFields();

    assignButtonsTestCase();

    assignPreconditionInputListener();

    // Send a request to get the sut variants and versions of the test case
    var urlJson = getTestURL().appendSegment("json").toString();
    xmlhttpJSON = new XMLHttpRequest();
    xmlhttpJSON.open("GET", urlJson, true);
    xmlhttpJSON.send();
    xmlhttpJSON.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            testcase = JSON.parse(this.responseText);
            var testCaseVersionIndex = (testcase.TestCaseVersions).length - {{ .TestCaseVersion.VersionNr }};
            // Save the versions of the loaded test case in selected versions.
            // These versions are selected by default in the list of versions.
            selectedVariants = testcase.TestCaseVersions[testCaseVersionIndex].Versions;

            // Deep copy the variants and versions of a test case.
            // They are needed for checking for changes in the selected versions.
            testCaseVariantData = jQuery.extend(true, {}, selectedVariants);

            updateSelectedVersions();
            // Update the version list with the versions of the selected variant
            updateVariantSelectionList(null);
        }
    };

    reloadDragAndDropFeature()
</script>
{{ end }}


