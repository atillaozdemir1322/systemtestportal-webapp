// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"reflect"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

func savePreconditionsCase(s xorm.Interface, version int64, preconditions []test.Precondition) error {
	for _, prec := range preconditions {
		prec = test.Precondition{
			TestVersion: version,
			Content:     prec.Content,
		}
		var err error
		if reflect.ValueOf(prec.Id) != reflect.ValueOf(test.Precondition{}.Id) {
			_, err = s.Table(casePreconditionTable).Where("Id = ?", prec.Id).Update(prec)
		} else {
			_, err = s.Table(casePreconditionTable).Insert(prec)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func getPreconditionsCase(s xorm.Interface, version int64) (preconditions []test.Precondition, err error) {
	err = s.Table(casePreconditionTable).Where("test_version = ?", version).Find(&preconditions)
	if err != nil {
		return nil, err
	}
	return preconditions, err
}

func getPreconditionCaseById(s xorm.Interface, id int64) (precondition test.Precondition, err error) {
	_, err = s.Table(casePreconditionTable).Where("id = ?", id).Get(&precondition)
	if err != nil {
		return test.Precondition{}, err
	}
	return precondition, err
}

func savePreconditionsSequence(s xorm.Interface, version int64, preconditions []test.Precondition) error {
	for _, prec := range preconditions {
		prec = test.Precondition{
			TestVersion: version,
			Content:     prec.Content,
		}
		var err error
		if reflect.ValueOf(prec.Id) != reflect.ValueOf(test.Precondition{}.Id) {
			_, err = s.Table(sequencePreconditionTable).Where("Id = ?", prec.Id).Update(prec)
		} else {
			_, err = s.Table(sequencePreconditionTable).Insert(prec)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func getPreconditionsSequence(s xorm.Interface, version int64) (preconditions []test.Precondition, err error) {
	err = s.Table(sequencePreconditionTable).Where("test_version = ?", version).Find(&preconditions)
	if err != nil {
		return nil, err
	}
	return preconditions, err
}
