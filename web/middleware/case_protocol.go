/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package middleware

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

const ProtocolKey = "protocol"

const (
	nonExistentProtocolTitle = "The protocol you requestsed doesn't exist"
	nonExistentProtocol      = "It seems that you requested a protocol that doesn't exist anymore or has never existed"
)

// CaseProtocolStore provides an interface for retrieving both
// case and sequence protocols
type CaseProtocolStore interface {
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testcaseID id.TestID) ([]test.CaseExecutionProtocol, error)
}

// CaseProtocol is a middleware that can retrieve a protocol from a request.
// It requires the container, the project and the testcase middleware
// to work.
func CaseProtocol(store CaseProtocolStore) negroni.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		protocolString, err := getParam(r, ProtocolKey)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		protocolNr, err := strconv.Atoi(protocolString)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		testID, err := getTestCaseID(r.Context().Value(TestCaseKey), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		protocol, err := getCaseProtocol(store, testID, protocolNr, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		AddToContext(r, ProtocolKey, protocol)
		next(w, r)
	}
}

func getTestCaseID(v interface{}, r *http.Request) (id.TestID, error) {
	tc, ok := v.(*test.Case)
	if ok {
		return tc.ID(), nil
	}
	return id.TestID{}, internalError(fmt.Sprintf("Given case is not a case, but <%+v>", v), nil, r)
}

func getCaseProtocol(store CaseProtocolStore, testID id.TestID, protocolNr int, r *http.Request) (*test.CaseExecutionProtocol, error) {
	protocols, err := store.GetCaseExecutionProtocols(testID)
	if err != nil {
		return nil, err
	}
	for _, protocol := range protocols {
		if protocol.ProtocolNr == protocolNr {
			return &protocol, nil
		}
	}
	return nil,
		errors.ConstructStd(http.StatusNotFound, nonExistentProtocolTitle, nonExistentProtocol, r).
			WithLogf("Client request non-existent protocol %+v for case %+v.", protocolNr, testID).
			WithStackTrace(1).
			Finish()
}
