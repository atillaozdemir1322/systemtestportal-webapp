/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package duplication

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

func SequencePost(ta handler.TestSequenceAdder, sequenceChecker id.TestExistenceChecker) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)

		if c.Project == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DuplicateSequence {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		dupTs := *c.Sequence
		newName := r.FormValue(httputil.TestSequenceName)
		if newName != c.Sequence.Name {
			dupTs.Rename(newName)
		}

		newVersion := dupTs.SequenceVersions[0]
		newVersion.IsMinor = false
		newVersion.Message = "Duplicated from \"" + c.Sequence.Name + "\""
		newVersion.VersionNr = len(dupTs.SequenceVersions) + 1
		dupTs.SequenceVersions = append([]test.SequenceVersion{newVersion}, dupTs.SequenceVersions...)
		for i := range dupTs.SequenceVersions {
			dupTs.SequenceVersions[i].Testsequence = dupTs.ID()
		}

		if err := dupTs.ID().Validate(sequenceChecker); err != nil {
			errors.Handle(err, w, r)
			return
		}

		if err := cutNewerSequenceVersions(&dupTs, getFormValueVersion(r)+1); err != nil {
			errors.Handle(err, w, r)
			return
		}

		err := ta.Add(&dupTs)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		// update the SequenceInfo so dynamic data is included
		tsv, err := test.UpdateInfo(&newVersion)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		httputil.SetHeaderValue(w, httputil.NewName, dupTs.ItemName())
		w.WriteHeader(http.StatusCreated)

		ctx := context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.TestSequence, dupTs).
			With(context.TestSequenceVersion, newVersion).
			With(context.Tasks, nil).
			With(context.SequenceInfo, tsv.SequenceInfo).
			With(context.Comments, nil).
			With(context.DeleteTestSequence, modal.TestSequenceDeleteMessage)
		handler.PrintTmpl(ctx, display.GetTabTestSequenceShow(r), w, r)
	}
}

func cutNewerSequenceVersions(ts *test.Sequence, to int) error {
	latest := len(ts.SequenceVersions)
	ver := to
	if ver <= 0 || ver > latest {
		return handler.InvalidTCVersion()
	}
	// Versions are listed backwards
	verIndex := latest - ver
	// Copy current testcase versions and slice afterwards
	tsv := ts.SequenceVersions
	ts.SequenceVersions = tsv[verIndex:]
	return nil
}
