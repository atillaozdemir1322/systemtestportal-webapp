/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package printing

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CasesListGet handles the showing of the template to print all test cases
func CasesListGet(lister handler.TestCaseLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcs, err := lister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getPrintCaseListTemplate(r)

		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.TestCases, tcs), tmpl, w, r)
	}
}

// getPrintCaseListTemplate returns either only the test cases
// print fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getPrintCaseListTemplate(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesListPrintFragment(r)
	}
	return getTabTestCasesListPrintTree(r)
}

// getTabTestCasesListPrintTree returns the test case print tab template with all parent templates
func getTabTestCasesListPrintTree(r *http.Request) *template.Template {
	return handler.GetPrintTree(r).
		// Tab testcase history tree
		Append(templates.PrintTestCases).
		Get().Lookup(templates.HeaderPrint)
}

// getTabTestCasesListPrintFragment returns only the test case print template
func getTabTestCasesListPrintFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).Append(templates.PrintTestCases).Get().Lookup(templates.TabContent)
}

// CaseGet handles the showing of the template to print all test cases
func CaseGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Case == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	printTestCase(c.Project, c.Case, w, r)
}

// printTestCase tries to respond with the print page for a testcase.
// If an error occurs an error response is sent instead.
func printTestCase(p *project.Project, tc *test.Case, w http.ResponseWriter, r *http.Request) {
	tmpl := getPrintCaseTemplate(r)
	tcv, err := handler.GetTestCaseVersion(r, tc)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	ctx := context.New().
		WithUserInformation(r).
		With(context.Project, p).
		With(context.TestCase, tc).
		With(context.TestCaseVersion, tcv).
		With(context.DurationHours, int(tcv.Duration.Hours())).
		With(context.DurationMin, tcv.Duration.GetMinuteInHour()).
		With(context.DurationSec, tcv.Duration.GetSecondInMinute())
	handler.PrintTmpl(ctx, tmpl, w, r)
}

// getPrintCaseTemplate returns either only the test case print fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getPrintCaseTemplate(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasePrintFragment(r)
	}
	return getTabTestCasePrintTree(r)
}

// getTabTestCasePrintTree returns the test case print tab template with all parent templates
func getTabTestCasePrintTree(r *http.Request) *template.Template {
	return handler.GetPrintTree(r).
		// Tab testcase history tree
		Append(templates.PrintCase).
		Get().Lookup(templates.HeaderPrint)
}

// getTabTestCasePrintFragment returns only the test case print template
func getTabTestCasePrintFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).Append(templates.PrintCase).Get().Lookup(templates.TabContent)
}
