/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

/*
CommentPut updates a comment and prints the new fragment back.
*/
func CommentPut(commentStore handler.Comments) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		context := handler.GetContextEntities(r)
		if context.User == nil {
			errors.Handle(context.Err, w, r)
			return
		}

		// Get the requested ID and text
		commentId, err := strconv.ParseInt(r.FormValue(httputil.CommentId), 10, 64)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		commentText := r.FormValue(httputil.CommentText)

		// Get the requested comment from the db
		comment, err := commentStore.GetComment(commentId, context.Project)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		// Check if the user requesting the edit is the author
		if comment.AuthorId != context.User.Name {
			errors.Handle(fmt.Errorf("%s", "You are not the Author of that comment"), w, r)
			return
		}

		// Update the comments fields
		comment.IsEdit = true
		comment.Text = commentText
		comment.Requester = context.User

		// And update it in the DB
		err = commentStore.UpdateComment(comment)
		if err != nil {
			errors.Handle(err, w, r)
		}

		// Finally return a new rendered comment
		err = creation.WriteComment(comment, w, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}
