/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/


package display

import (
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"net/http"
	"testing"
)

func TestActivityGet(t *testing.T) {
	body := "{\"text\":\"test\"}"
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
		},
	)

	dummyActivityStore := handler.ActivityMock{}

	handler.Suite(t,
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				ActivityGet(dummyActivityStore),
				handler.HasStatus(http.StatusOK),
			),
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTest("No Project",
			handler.ExpectResponse(
				ActivityGet(dummyActivityStore),
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.NewRequest(ctxNoProject, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctxNoProject, http.MethodPost, handler.NoParams, body),
		),
	)
}

func TestActivityPut(t *testing.T) {
	body := "{\"offsetCount\":\"1\"}]}"
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)

	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:    handler.DummyUser,
		},
	)

	dummyActivityStore := handler.ActivityMock{}
	handler.Suite(t,
		handler.CreateTest("Normal case no db entries",
			handler.ExpectResponse(
				ActivityPut(dummyActivityStore),
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.NewRequest(ctx, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctx, http.MethodPost, handler.NoParams, body),
		),
		handler.CreateTest("No Project",
			handler.ExpectResponse(
				ActivityPut(dummyActivityStore),
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.NewRequest(ctxNoProject, http.MethodPost, handler.NoParams, body),
			handler.NewFragmentRequest(ctxNoProject, http.MethodPost, handler.NoParams, body),
		),
	)
}



